import axios from 'axios';
import { PlaceholderPost } from './models';

export const placeholderApi = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
});

export async function getPosts(): Promise<PlaceholderPost[]> {
  const result = await placeholderApi.get('/posts');
  return result.data as PlaceholderPost[];
}
