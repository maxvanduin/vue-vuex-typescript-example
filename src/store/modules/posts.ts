
import { Module, VuexModule, Mutation, Action, getModule } from 'vuex-module-decorators';
import store from '@/store/index';
import { PlaceholderPost } from '../models';
import { getPosts } from '@/store/api';

@Module({
  name: 'posts',
  namespaced: true,
  dynamic: true,
  store,
})
class Posts extends VuexModule {
  public posts: PlaceholderPost[] | null = null;

  @Mutation
  public updatePosts(posts: PlaceholderPost[]) {
    this.posts = posts;
  }

  @Action({ commit: 'updatePosts' })
  public async fetchPosts(): Promise<PlaceholderPost[]> {
    return await getPosts();
  }
}

export default getModule(Posts, store);
